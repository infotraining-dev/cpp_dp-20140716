#ifndef SHAPE_GROUP_HPP_
#define SHAPE_GROUP_HPP_

#include "shape.hpp"
#include "clone_factory.hpp"
#include <boost/bind.hpp>

namespace Drawing
{

class Bound
{
    int dx, dy;
public:
    Bound(int dx, int dy) : dx(dx), dy(dy) {}

    void operator()(Shape* s)
    {
        s->move(dx, dy);
    }
};

// TO DO: zaimplementowac kompozyt grupuj�cy kszta�ty geometryczne
class ShapeGroup : public Shape
{
    std::vector<Shape*> shapes_;
public:
    ShapeGroup()
    {
    }

    ShapeGroup(const ShapeGroup& source)
    {
        shapes_.resize(source.shapes_.size());
        std::transform(source.shapes_.begin(), source.shapes_.end(),
                       shapes_.begin(), boost::mem_fn(&Shape::clone));
    }

    void draw() const
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::mem_fn(&Shape::draw));
    }

    void move(int dx, int dy)
    {
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::move, _1, dx, dy));
    }

    ShapeGroup* clone() const
    {
        return new ShapeGroup(*this);
    }


    void add(Shape* shape)
    {
        shapes_.push_back(shape);
    }

    void read(std::istream &in)
    {
        int count;

        in >> count;

        for(int i = 0; i < count; ++i)
        {
            std::string type_identifier;
            in >> type_identifier;

            Shape* shape = ShapeFactory::instance().create(type_identifier);

            shape->read(in);

            add(shape);
        }
    }

    void write(std::ostream &out)
    {
        out << "ShapeGroup " << shapes_.size() << std::endl;
        std::for_each(shapes_.begin(), shapes_.end(), boost::bind(&Shape::write, _1, boost::ref(out)));
    }
};

}

#endif /*SHAPE_GROUP_HPP_*/
