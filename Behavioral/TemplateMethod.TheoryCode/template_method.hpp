#ifndef TEMPLATE_METHOD_HPP_
#define TEMPLATE_METHOD_HPP_

#include <iostream>
#include <string>
#include <memory>

class Product
{
public:
    virtual void run()
    {
        std::cout << "Product::run()" << std::endl;
    }
};

class BetterProduct : public Product
{
public:
    virtual void run()
    {
        std::cout << "BetterProduct::run()" << std::endl;
    }
};

// "AbstractClass"
class AbstractClass
{
protected:
	virtual void primitive_operation_1() = 0;
	virtual void primitive_operation_2() = 0;
    virtual std::unique_ptr<Product> create_product()
    {
        return std::unique_ptr<Product>(new Product);
    }

public:
	void template_method()
	{
		primitive_operation_1();

        std::unique_ptr<Product> p = create_product();
        p->run();

		primitive_operation_2();
		std::cout << std::endl;
	}
	
	virtual ~AbstractClass() {}
};

// "ConcreteClass"
class ConcreteClassA : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassA.PrimitiveOperation2()" << std::endl;
	}
};

// "ConcreteClass"
class ConcreteClassB : public AbstractClass
{
protected:
	void primitive_operation_1()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation1()" << std::endl;
	}

	void primitive_operation_2()
	{
		std::cout << "ConcreteClassB.PrimitiveOperation2()" << std::endl;
	}

    virtual std::unique_ptr<Product> create_product()
    {
        return std::unique_ptr<Product>(new BetterProduct);
    }


};

#endif /*TEMPLATE_METHOD_HPP_*/
