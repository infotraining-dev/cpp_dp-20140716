/*
 * square.hpp
 *
 *  Created on: 04-02-2013
 *      Author: Krystian
 */

#ifndef SQUARE_HPP_
#define SQUARE_HPP_

#include "shape.hpp"

// TODO: Doda� klase Square
namespace Drawing
{
    class Square : public ShapeBase
    {
    private:
        unsigned int side_;
    public:
        Square(int x = 0, int y = 0, size_t radius = 0) : ShapeBase(x, y), side_(radius)
        {
        }

        unsigned int side() const
        {
            return side_;
        }

        void set_side(unsigned int side)
        {
            side_ = side;
        }

        virtual void draw() const
        {
            std::cout << "Drawing a square at: " << point() << " size: " << side_ << std::endl;
        }

        virtual void read(std::istream& in)
        {
            Point pt;
            unsigned int side;

            in >> pt >> side;

            set_point(pt);
            set_side(side);
        }

        virtual void write(std::ostream& out)
        {
            out << "Circle " << point() << " " << side() << std::endl;
        }
    };
}


#endif /* SQUARE_HPP_ */
