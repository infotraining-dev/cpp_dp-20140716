# The set of languages for which implicit dependencies are needed:
SET(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
SET(CMAKE_DEPENDS_CHECK_CXX
  "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/before.cpp" "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/build/CMakeFiles/FactoryMethod.Exercise1.Alt.dir/before.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/employee.cpp" "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/build/CMakeFiles/FactoryMethod.Exercise1.Alt.dir/employee.cpp.o"
  "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/hrinfo.cpp" "/home/developer/Szkolenie/CPP_DP 20140716/Creational/FactoryMethod.Exercise1.Alt/build/CMakeFiles/FactoryMethod.Exercise1.Alt.dir/hrinfo.cpp.o"
  )
SET(CMAKE_CXX_COMPILER_ID "GNU")

# Targets to which this target links.
SET(CMAKE_TARGET_LINKED_INFO_FILES
  )

# The include file search paths:
SET(CMAKE_C_TARGET_INCLUDE_PATH
  )
SET(CMAKE_CXX_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_Fortran_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
SET(CMAKE_ASM_TARGET_INCLUDE_PATH ${CMAKE_C_TARGET_INCLUDE_PATH})
