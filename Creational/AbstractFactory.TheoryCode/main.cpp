#include <iostream>
#include "abstract_factory.hpp"

using namespace std;

int main()
{

	// Abstract factory #1
	AbstractFactory* factory1 = new ConcreteFactory1() ;
	Client client1(factory1);
	client1.Run();

	delete factory1;

	// Abstract factory #2
	ConcreteFactory2 factory2;
	Client client2(&factory2);
	client2.Run();
}
