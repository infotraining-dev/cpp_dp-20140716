#ifndef HRINFO_HPP_
#define HRINFO_HPP_

#include "employee.hpp"

class HRInfo
{
protected:
	const Employee* employee_;
public:
	HRInfo(const Employee* e);
	virtual void info() const = 0;
	virtual ~HRInfo();
};

class StdInfo : public HRInfo
{
public:
	StdInfo(const Employee* e);

	virtual void info() const;
};

class TempInfo : public HRInfo
{
public:
	TempInfo(const Employee* e); 

	virtual void info() const;
};

#endif /* HRINFO_HPP_ */
